package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.ISessionService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Session;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Category(DBCategory.class)
public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final ISessionService SESSION_SERVICE = new SessionService(CONNECTION_SERVICE);

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final Date TODAY = new Date();

    @NotNull
    private List<Session> sessions = new ArrayList<>();

    @NotNull
    private final List<Session> alfaSessions = new ArrayList<>();

    @NotNull
    private final Comparator<Session> comparator = (o1, o2) -> {
        if (o1 == o2) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    };

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Session session = new Session();
            session.setCreated(TODAY);
            if (i <= 5) {
                session.setUserId(USER_ALFA_ID);
                alfaSessions.add(session);
            } else {
                session.setUserId(USER_BETA_ID);
            }
            sessions.add(session);
            SESSION_SERVICE.add(session);
        }
        sessions = sessions.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() {
        SESSION_SERVICE.removeAll();
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAdd_ModelNotFoundException_1() throws AbstractException {
        SESSION_SERVICE.add(USER_ALFA_ID, null);
    }

    @Test
    public void testFindAllUser() throws AbstractException {
        assertEquals(alfaSessions, SESSION_SERVICE.findAll(USER_ALFA_ID, null));
        assertEquals(sessions, SESSION_SERVICE.findAll());
        final Comparator<Session> comparator = null;
        assertEquals(sessions, SESSION_SERVICE.findAll(comparator));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_UserIdEmptyException_1() throws AbstractException {
        SESSION_SERVICE.findAll("", null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_UserIdEmptyException_2() throws AbstractException {
        SESSION_SERVICE.findAll(NULL_STR, null);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindOneById_ModelNotFoundException_1() throws AbstractException {
        SESSION_SERVICE.findOneById(NONE_STR, NONE_STR);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindOneById_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.removeAll();
        SESSION_SERVICE.findOneById(NONE_STR, NONE_STR);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOne_ModelNotFoundException_1() throws AbstractException {
        SESSION_SERVICE.removeOne(USER_ALFA_ID, null);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOne_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.removeOne(USER_ALFA_ID, new Session());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOneById_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.removeOneById(USER_ALFA_ID, NONE_STR);
    }

    @Test
    public void testAdd() throws AbstractException {
        final Session newSession = new Session();
        newSession.setUserId(NONE_STR);
        SESSION_SERVICE.add(newSession);
        sessions.add(newSession);
        assertEquals(sessions.size(), SESSION_SERVICE.getSize());
        assertEquals(sessions, SESSION_SERVICE.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAdd_ModelNotFoundException_2() throws AbstractException {
        SESSION_SERVICE.add(null);
    }


    @Test
    public void testAddAll() throws AbstractException {
        final List<Session> newSessions = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            Session session = new Session();
            session.setUserId(NONE_STR);
            newSessions.add(session);
        }
        SESSION_SERVICE.addAll(newSessions);
        sessions.addAll(newSessions);
        assertEquals(sessions.size(), SESSION_SERVICE.getSize());
        assertEquals(sessions, SESSION_SERVICE.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAddAll_ModelNotFoundException() throws AbstractException {
        SESSION_SERVICE.addAll(null);
    }

    @Test
    public void testSet() throws AbstractException {
        final List<Session> newSessions = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            Session session = new Session();
            session.setUserId(NONE_STR);
            newSessions.add(session);
        }
        SESSION_SERVICE.set(newSessions);
        sessions.addAll(newSessions);
        assertEquals(newSessions.size(), SESSION_SERVICE.getSize());
        assertEquals(newSessions, SESSION_SERVICE.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testSet_ModelNotFoundException() throws AbstractException {
        SESSION_SERVICE.set(null);
    }

    @Test
    public void testRemoveAll() throws AbstractException {
        final List<Session> nullSessions = null;
        final List<Session> newSessions = new ArrayList<>();
        SESSION_SERVICE.removeAll(nullSessions);
        assertEquals(sessions.size(), SESSION_SERVICE.getSize());

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Session session = new Session();
            session.setUserId(NONE_STR);
            newSessions.add(session);
        }
        SESSION_SERVICE.addAll(newSessions);
        assertNotEquals(0, SESSION_SERVICE.getSize());
        SESSION_SERVICE.removeAll(newSessions);
        assertEquals(0, SESSION_SERVICE.getSize(NONE_STR));
    }

}
