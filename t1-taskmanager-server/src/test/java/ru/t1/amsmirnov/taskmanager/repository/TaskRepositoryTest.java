package ru.t1.amsmirnov.taskmanager.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final Project projectUserAlfa = new Project();

    @NotNull
    private SqlSession connection;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private List<Task> alfaTasks;

    @NotNull
    private List<Task> betaTasks;

    @NotNull
    private ITaskRepository taskRepository;

    @BeforeClass
    public static void initData() {
        projectUserAlfa.setName("Test task name");
        projectUserAlfa.setDescription("Test task description");
        projectUserAlfa.setUserId(USER_ALFA_ID);
    }

    @Before
    public void initRepository() throws Exception {
        tasks = new ArrayList<>();
        connection = CONNECTION_SERVICE.getConnection();
        taskRepository = connection.getMapper(ITaskRepository.class);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Task task = new Task();
            task.setName("Task : " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("Description: " + i);
            if (i <= 5)
                task.setUserId(USER_ALFA_ID);
            else
                task.setUserId(USER_BETA_ID);
            if (i % 3 == 0)
                task.setProjectId(projectUserAlfa.getId());
            try {

                tasks.add(task);
                taskRepository.add(task);
            } catch (final Exception e) {
                connection.rollback();
                throw e;
            }
        }
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() throws Exception {
        try {
            taskRepository.removeAll();
            tasks.clear();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }


    @Test
    public void testAdd() throws Exception {
        @NotNull final Task task = new Task();
        task.setName("Test task name");
        task.setDescription("Test task description");
        task.setUserId(USER_ALFA_ID);
        try {
            taskRepository.add(task);
            tasks.add(task);
            List<Task> actualTasks = taskRepository.findAll();
            assertEquals(tasks, actualTasks);
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAll() throws Exception {
        try {
            assertNotEquals(0, taskRepository.getSize());
            taskRepository.removeAll();
            assertEquals(0, taskRepository.getSize());
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        try {
            assertNotEquals(0, taskRepository.getSizeUserId(USER_ALFA_ID));
            taskRepository.removeAllUserId(USER_ALFA_ID);
            assertEquals(0, taskRepository.getSizeUserId(USER_ALFA_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testExistById() throws Exception {
        try {
            for (final Task task : tasks) {
                assertTrue(taskRepository.existByIdUserId(task.getUserId(), task.getId()));
            }
            assertFalse(taskRepository.existByIdUserId(USER_ALFA_ID, UUID.randomUUID().toString()));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAll() throws Exception {
        try {
            assertNotEquals(taskRepository.findAllUserId(USER_ALFA_ID), taskRepository.findAllUserId(USER_BETA_ID));

            assertEquals(alfaTasks, taskRepository.findAllUserId(USER_ALFA_ID));
            assertEquals(betaTasks, taskRepository.findAllUserId(USER_BETA_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Ignore
    public void testFindAllComparator() throws Exception {
        try {
            alfaTasks.sort(TaskSort.BY_NAME.getComparator());
            assertEquals(alfaTasks, taskRepository.findAllSortedUserId(USER_ALFA_ID, "name"));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneById() throws Exception {
        try {
            for (final Task task : tasks) {
                assertEquals(task, taskRepository.findOneByIdUserId(task.getUserId(), task.getId()));
            }
            assertNull(taskRepository.findOneById(NONE_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testGetSize() throws Exception {
        try {
            assertEquals(tasks.size(), taskRepository.getSize());
            final long alfaTasksCount = tasks
                    .stream()
                    .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                    .count();
            final long betaTasksSize = tasks
                    .stream()
                    .filter(t -> t.getUserId().equals(USER_BETA_ID))
                    .count();
            assertEquals(alfaTasksCount, taskRepository.getSizeUserId(USER_ALFA_ID));
            assertEquals(betaTasksSize, taskRepository.getSizeUserId(USER_BETA_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveById() throws Exception {
        try {
            for (final Task task : tasks) {
                assertTrue(taskRepository.existById(task.getId()));
                taskRepository.removeOneByIdUserId(task.getUserId(), task.getId());
                assertFalse(taskRepository.existById(task.getId()));
            }
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAllByProjectId() throws Exception {
        final List<Task> alfaTasksWithProject = alfaTasks
                .stream()
                .filter(t -> projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> betaTasksWithProject = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID) && projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        try {
            final List<Task> alfaRepoTasks = taskRepository.findAllByProjectId(USER_ALFA_ID, projectUserAlfa.getId());
            final List<Task> betaRepoTasks = taskRepository.findAllByProjectId(USER_BETA_ID, projectUserAlfa.getId());
            assertNotEquals(alfaRepoTasks, betaRepoTasks);
            assertEquals(alfaTasksWithProject, alfaRepoTasks);
            assertEquals(betaTasksWithProject, betaRepoTasks);

            assertEquals(new ArrayList<>(), taskRepository.findAllByProjectId(USER_ALFA_ID, NONE_ID));
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testUpdate() throws Exception {
        assertEquals(tasks.get(0), taskRepository.findOneById(tasks.get(0).getId()));
        tasks.get(0).setName("UPDATED_NAME");
        taskRepository.update(tasks.get(0));
        assertEquals(tasks.get(0).getName(), taskRepository.findOneById(tasks.get(0).getId()).getName());
    }

}
