package ru.t1.amsmirnov.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, user_id, created, role)" +
            " VALUES (#{id}, #{userId}, #{created}, #{role})")
    void add(@NotNull Session session);

    @Nullable
    @Select("SELECT * FROM tm_session ORDER BY created")
    @Result(property = "userId", column = "user_id")
    List<Session> findAll();

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} ORDER BY created")
    @Result(property = "userId", column = "user_id")
    List<Session> findAllUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id}")
    @Result(property = "userId", column = "user_id")
    Session findOneById(
            @NotNull @Param("id") String id
    );

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    @Result(property = "userId", column = "user_id")
    Session findOneByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT COUNT(1) FROM tm_session")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_session WHERE user_id = #{userId}")
    int getSizeUserId(@NotNull String userId);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_session WHERE id = #{id})")
    boolean existById(
            @NotNull @Param("id") String id
    );

    @Select("SELECT EXISTS(SELECT 1 FROM tm_session WHERE user_id = #{userId} AND id = #{id})")
    boolean existByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Update("UPDATE tm_session SET created = #{created}," +
            " role = #{role}," +
            " user_id = #{userId}" +
            " WHERE id = #{id}")
    void update(@NotNull Session session);

    @Delete("DELETE FROM tm_session")
    void removeAll();

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void removeAllUserId(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeOneById(
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeOneByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

}
