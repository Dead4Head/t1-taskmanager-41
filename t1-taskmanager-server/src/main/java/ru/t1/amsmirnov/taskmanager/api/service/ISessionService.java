package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
