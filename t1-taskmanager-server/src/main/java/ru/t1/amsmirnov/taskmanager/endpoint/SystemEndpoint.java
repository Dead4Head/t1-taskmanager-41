package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest serverAboutRequest
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse serverAboutResponse = new ServerAboutResponse();
        serverAboutResponse.setEmail(propertyService.getAuthorEmail());
        serverAboutResponse.setName(propertyService.getAuthorName());
        return serverAboutResponse;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest serverVersionRequest
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse serverVersionResponse = new ServerVersionResponse();
        serverVersionResponse.setVersion(propertyService.getApplicationVersion());
        return serverVersionResponse;
    }

}
