package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

public final class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse() {
    }

    public TaskCompleteByIdResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskCompleteByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
