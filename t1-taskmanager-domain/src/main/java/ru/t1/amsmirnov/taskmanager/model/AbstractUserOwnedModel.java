package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @Column(name = "user_id")
    protected String userId;

    public AbstractUserOwnedModel() {
    }

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@Nullable final String userId) {
        this.userId = userId;
    }

}
