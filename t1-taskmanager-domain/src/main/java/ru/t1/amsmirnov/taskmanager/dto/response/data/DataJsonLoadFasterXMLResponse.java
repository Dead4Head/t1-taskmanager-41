package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataJsonLoadFasterXMLResponse extends AbstractResultResponse {

    public DataJsonLoadFasterXMLResponse() {
    }

    public DataJsonLoadFasterXMLResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
