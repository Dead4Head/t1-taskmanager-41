package ru.t1.amsmirnov.taskmanager.dto.request.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ProjectRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectRemoveByIdRequest() {
    }

    public ProjectRemoveByIdRequest(
            @Nullable final String token,
            @Nullable final String id
    ) {
        super(token);
        setId(id);
    }

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable final String id) {
        this.id = id;
    }

}