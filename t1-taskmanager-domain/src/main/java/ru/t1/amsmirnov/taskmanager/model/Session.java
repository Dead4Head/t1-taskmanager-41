package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

import javax.persistence.*;

@Entity
@Table(name = "tm_session")
public final class Session extends AbstractUserOwnedModel {

    @Nullable
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Nullable
    public Role getRole() {
        return role;
    }

    public void setRole(@Nullable final Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return getId() + " " + getCreated();
    }

}
