package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.*;
import ru.t1.amsmirnov.taskmanager.dto.response.data.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull String NAME = "DomainEndpoint";

    @NotNull String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(@WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBase64LoadRequest request);

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataBase64SaveRequest request);

    @NotNull
    @WebMethod
    DataBinLoadResponse loadDataBin(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataBinLoadRequest request);

    @NotNull
    @WebMethod
    DataBinSaveResponse saveDataBin(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataBinSaveRequest request);

    @NotNull
    @WebMethod
    DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataJsonLoadFasterXMLRequest request);

    @NotNull
    @WebMethod
    DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataJsonSaveFasterXMLRequest request);

    @NotNull
    @WebMethod
    DataJsonLoadJaxbResponse loadDataJsonJaxB(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataJsonLoadJaxbRequest request);

    @NotNull
    @WebMethod
    DataJsonSaveJaxbResponse saveDataJsonJaxB(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataJsonSaveJaxbRequest request);

    @NotNull
    @WebMethod
    DataXmlLoadFasterXMLResponse loadDataXmlFasterXML(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataXmlLoadFasterXMLRequest request);

    @NotNull
    @WebMethod
    DataXmlSaveFasterXMLResponse saveDataXmlFasterXML(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataXmlSaveFasterXMLRequest request);

    @NotNull
    @WebMethod
    DataXmlLoadJaxBResponse loadDataXmlJaxB(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    @WebMethod
    DataXmlSaveJaxBResponse saveDataXmlJaxB(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    @WebMethod
    DataYamlLoadFasterXMLResponse loadDataYAMLFasterXML(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataYamlLoadFasterXMLRequest request);

    @NotNull
    @WebMethod
    DataYamlSaveFasterXMLResponse saveDataYAMLFasterXML(@WebParam (name = REQUEST, partName = REQUEST) @NotNull DataYamlSaveFasterXMLRequest request);

}
