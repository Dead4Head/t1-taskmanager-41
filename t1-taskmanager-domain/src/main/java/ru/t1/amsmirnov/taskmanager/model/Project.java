package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.model.IWBS;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.database.DBCost;

import javax.persistence.*;

@Entity
@Table(name = "tm_project")
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public Project() {
    }

    public Project(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return getName() + " : " + getDescription();
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

}
